#!/bin/bash

##########################################################################
# Сценарій для перенесення музики на кишенькову гралку. На жаль, моя     #
# гралка не розуміє ogg vorbis, тому при перенесенні цих та инших        #
# форматів перетворюю їх на mp2 з хвостом назви «.mp3». Таким чином я не #
# порушую патентів, але слухаю музику:)                                  #
##########################################################################

###########
# Функції #
###########

create_conf_file () {
    echo "$(basename $0) ще не налаштовано."
    until [ -n "$MOUNTPOINT" ]; do
        read -e -p "Будь ласка, вкажіть теку, де під'єднується ваша гралка: " MOUNTPOINT
    done
    echo "MOUNTPOINT=$MOUNTPOINT" > $CONFFILE
    echo "Дякую. Ви в будь-який момент зможете це змінити,"
    echo "відредагувавши файл $CONFFILE"
    echo "=== Налаштування завершено ==="
}

get_repository_prefix () {
    PREFIX="$(git rev-parse --show-prefix)"
    echo "$PREFIX"
}

convert_or_copy () {
    PREFIX="${MOUNTPOINT}/$(get_repository_prefix "$@")"
    TARGETDIR="${PREFIX}/$(dirname "$@")"
    TARGETFILE="${PREFIX}/$@"

    # Створити теку на гралці, якщо треба
    if [ ! -d "${TARGETDIR}" ]; then
        mkdir --parents "${TARGETDIR}"
    fi

    # Скопіювати / перетворити файли
    TYPE=$(file --mime-type --brief --dereference "$@")
    case $TYPE in
        *vorbis*|*ogg*|*flac*)
            echo "Копіювання з перетворенням: $@"
                  nice ${AVCONV} -i "$@" -f mp2 -ar 44100 -ab 256k -y "${TARGETFILE}.mp3" </dev/null
            ;;
        *)
            echo "Копіювання: $@"
            cp --dereference "$@" "${TARGETFILE}"
            ;;
        # *)
        #     echo "Пропуск: $@"
        #     ;;
    esac
}
##############
# Налаштунки #
##############
AVCONV=ffmpeg
CONFFILE=$HOME/.config/player.sh.conf
if [ -f $CONFFILE ]; then
    source $CONFFILE
else create_conf_file; fi

#######
# Дія #
#######
while [ $# -gt 0 ]; do
    find "$1" -type f -follow | sort | while read FILENAME; do convert_or_copy "$FILENAME"; done
    shift
done

exit
