#!/usr/bin/env python3
# License: cc0 https://creativecommons.org/publicdomain/zero/1.0/

import argparse
import subprocess
import re
from os import remove
from collections import defaultdict

parser = argparse.ArgumentParser(
    description="Pack tiffs into pdfs, based on common beginning of file name."
    "For instance, when called on three files:"
    "Violin001.tif Violin002.tif and Viola.tif"
    " it will produce two pdfs: Violin.pdf and Viola.pdf")

parser.add_argument('files', metavar='FILES', nargs='+',
                    help="Tiff files to operate on")


def tiffs2pdf(tiffs, target):
    tmp_tiff = target + ".tif"
    pdf = target + ".pdf"
    try:
        subprocess.run(["tiffcp"] + tiffs + [tmp_tiff], check=True)
        subprocess.run(["tiff2pdf", "-o", pdf, tmp_tiff], check=True)
    finally:
        remove(tmp_tiff)


def main():
    args = parser.parse_args()

    basenames = defaultdict(list)
    for tiff in args.files:
        match = re.search(r"(\D+[^\d\s]).*\.tif?", tiff, re.I)
        if not match:
            continue
        else:
            key = match.group(1)
            basenames[key].append(tiff)

    for target, tiffs in basenames.items():
        try:
            tiffs2pdf(tiffs, target)
            print("Created {}".format(target))
        except subprocess.CalledProcessError:
            print("Subprocess failed")
            exit(1)


if __name__ == '__main__':
    main()
